function plot_positions(c, alpha, beta)
    % funzione che plotta la configurazione istantanea del sistema

    global a b d
    
    a_vec = a*exp(1i*alpha);
    b_vec = b*exp(1i*beta);
    c_vec = c*exp(1i*pi);
    d_vec = d*exp(1i*3/2*pi);
    
    % punti notevoli
    
    O = [0, 0];
    B = [real(a_vec), imag(a_vec)];
    D = [real(a_vec + b_vec), imag(a_vec + b_vec)];
    E = [real(c_vec), imag(c_vec)];
    D_bis = [real(c_vec + d_vec), imag(c_vec + d_vec)];
    
    figure
    hold on
    title('configurazione del sistema - equazione di chiusura')
    
    plot([O(1) B(1)], [O(2) B(2)], 'Color', [190 0 0]/255, 'LineWidth', 2)
    plot([B(1) D(1)], [B(2) D(2)], 'Color', [235 85 25]/255, 'LineWidth', 2)
    plot([O(1) E(1)], [O(2) E(2)], 'Color', [55 105 235]/255, 'LineWidth', 2)
    plot([E(1) D_bis(1)], [E(2) D_bis(2)], 'Color', [0 0 130]/255, 'LineWidth', 2)
    legend('a', 'b', 'c', 'd');
    grid on
    axis equal
    xlim([-.6 .1])
    ylim([-.3 .1])
    
end

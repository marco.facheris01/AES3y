Copyright (C) 2009

Pierangelo Masarati <masarati@aero.polimi.it>

Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
via La Masa, 34 - 20156 Milano, Italy
http://www.aero.polimi.it/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (version 2 of the License).

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   --- o --- o ---

Gli script lab01dir.m e lab01inv.m consentono di studiare rispettivamente
la cinematica diretta ed inversa di un manipolatore piano a due gradi di
liberta'.  Quindi ne studiano la dinamica inversa e verificano che il
movimento conseguente sia in accordo con quello imposto sia direttamente
che inversamente.

Tali script fanno uso di:

lab01_data.m	dati del problema
lab01inv_nr.m	residuo/matrice Jacobiana dei vincoli per cinematica inversa
lab01dir_func.m	valutazione funzione per dinamica diretta

Gli script sono compatibili con octave e matlab.


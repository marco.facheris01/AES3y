function [F]=quadrilatero(x)

global al0 de0 a b c d 

%Chiusura cinematica delle posizioni, espressa come F(x)=0

F(1) = a*cos(al0) + b*cos(x(1)) - c*cos(x(2)) - d*cos(de0);  
F(2) = a*sin(al0) + b*sin(x(1)) - c*sin(x(2)) - d*sin(de0);

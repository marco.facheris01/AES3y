function [v1,v2,v3,om1,om2,lm,h1,h3] = cinematica(theta)

% Questa funzione consente di ricavare le relazioni cinematiche che
% descrivono il moto del sistema

global L R

v1 = L^2;                               % velocit� centro asta A
v2=-2*L*sin(theta);                     % velocit� centro disco
v3=4*L^2;                               % velocit� massa concentrata

om1=1;                                  % velocit� angolare asta A
om2 = 2/R*L*sin(theta);                 % velocit� angolare disco

lm=L*cos(theta);                        % lunghezza molla deformata

h1 = L*sin(theta);                      % baricentro asta A
h3=2*L*sin(theta);                      % baricentro massa concentrata
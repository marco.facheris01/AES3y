clear all
close all
clc

global g
global L R
global m1 m2 m3 J1 J2
global k r
global C omega
global v1 v2 v3 om1 om2 lm0 h1 h2 dl0
global theta_0

%Dati
g=9.81;
L=1;
R=0.2;
m1=5;
m2=3;
m3=2;
J1=(1/12)*m1*(2*L)^2;
J2=0.5*m2*R^2;
k=500;
r=20;
theta_0=pi/4;

%Dati di simulazione
t_0=0;
t_f=200;
x_0 = [0;theta_0+pi/12]; % vettore condizioni iniziali

%Forzamento
C=5;
omega=10; % Pulsazione forzante

%Andamento forzamento nel tempo
figure
plot([t_0:0.01:t_f],C*cos(omega*[t_0:0.01:t_f]),'b','Linewidth',2');
xlabel('tempo [s]');
ylabel('Coppia [Nm]');
grid on;

%Equilibrio e precarico
theta_0 = pi/4;
[v1,v2,v3,om1,om2,lm0,h1,h2] = cinematica(theta_0);
dl0=precarico(theta_0);

%Integrazione numerica
[t_nl,y_nl]=ode45(@sistema_non_lineare,[t_0 t_f],x_0);

% Idem per sistema linearizzato nell'intorno di theta_0
[t_l,y_l]=ode45(@sistema_lineare,[t_0 t_f],x_0);

figure
plot(t_nl,rad2deg(y_nl(:,2)),'b','Linewidth',2);
hold on
plot(t_l,rad2deg(y_l(:,2)),'g','Linewidth',2);
xlabel('tempo [s]');
ylabel('\theta [deg]');
legend('Non lineare','Lineare');
grid on;

%% Studio nel dominio delle frequenze
%Sistema linearizzato
[M_eq,~] = energia_cinetica(v1,v2,v3,om1,om2,theta_0);
[dlm,ddlm,dh1,dh3,ddh1,ddh3] = energia_potenziale(theta_0);
K_eq=k*dl0*ddlm+k*dlm^2+m1*g*ddh1+m3*g*ddh3;
R_eq=r*dlm^2;

omega_nat=sqrt(K_eq/M_eq);
freq_nat=omega_nat/(2*pi);
h=r/(2*M_eq*omega_nat);

%Risposta in frequenza
omega_vect=0:0.01:100;
FRF=C./(-M_eq*omega_vect.^2+1i*omega_vect*R_eq+K_eq);

figure
subplot(2,1,1);
plot(omega_vect,abs(FRF),'b','Linewidth',2);
xlabel('pulsazione [rad/s]');
ylabel('Modulo [rad]');
grid on;
subplot(2,1,2);
plot(omega_vect,rad2deg(angle(FRF)),'b','Linewidth',2);
xlabel('pulsazione [rad/s]');
ylabel('fase [deg]');
grid on

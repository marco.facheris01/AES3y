function dl0 = precarico(theta_0)

% Questa funzione consente di ricavare il precarico statico della molla

global g
global L
global m1 m3 
global k 


dl0 = g*(m1*L*cos(theta_0)+m3*2*L*cos(theta_0))/(k*L*sin(theta_0)); 

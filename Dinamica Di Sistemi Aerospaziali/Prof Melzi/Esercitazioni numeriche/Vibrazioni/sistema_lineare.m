function dy = sistema_lineare(t,y)

global g
global m1 m3 L R
global k r
global C omega
global v1 v2 v3 om1 om2 h1 h3 dl0
global theta_0


thetap = y(1);
theta = y(2);

[v1,v2,v3,om1,om2,lm,h1,h3] = cinematica(theta_0);

[M,dM] = energia_cinetica(v1,v2,v3,om1,om2,theta_0);

[dlm,ddlm,dh1,dh3,ddh1,ddh3] = energia_potenziale(theta_0);
dV = (k*dl0*ddlm+k*dlm^2+m1*g*ddh1+m3*g*ddh3)*(theta-theta_0);
dD = r*dlm^2*thetap;
Q=(2*C*L*sin(theta_0)/R)*cos(omega*t);

dy = [-(+dD+dV-Q)/M;
      thetap];
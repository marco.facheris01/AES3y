function [dlm,ddlm,dh1,dh3,ddh1,ddh3] = energia_potenziale(theta)

% Questa funzione ricava le derivate parziali per i termini legati
% all'energia potenziale del sistema
global L 

dlm=-L*sin(theta);
ddlm=-L*cos(theta);

dh1=L*cos(theta);
ddh1=-L*sin(theta);

dh3=2*L*cos(theta);
ddh3=-2*L*sin(theta);

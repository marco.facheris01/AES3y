%% ESERCITAZIONE 14
% Calcolare linee di corrente e campo velocità attorno ad un cerchio non
% portante con coordinate centro: x_c=0, y_c=1. raggio a=0.5

% come cambia se ci metto una parete solida?

% metodo delle immagini



U_inf = 1; % freestream velocity
R = 0.5;
x_c = 0;
y_c = 1;

h = .2; %discretization step

x_circ = [-R:.01:R];

% x^2+y^2=R^2   ->
y_up = @(x) sqrt(R^2-(x-x_c).^2) + y_c;
y_low = @(x) -sqrt(R^2-(x-x_c).^2) + y_c;

plot(x_circ, y_up(x_circ), 'b');
hold on
plot(x_circ,y_low(x_circ), 'b');
axis equal



% calcolo di b e lambda per avere le l ed h desiderate
% bisogna risolvere un sistema NON LINEARE, con vettore incognite x = [x(1); x(2)]
%b_lambda = @(x) [  (l^2/x(1)^2 - (1+x(2)/(pi*v_inf*x(1))))  ; ( h/x(1)*tan( (h/x(1)) / (x(2)/(pi*v_inf*x(1))) ) -1)];
b_lambda = @(x) [  (1/x(1)^2 - (1+x(2)/(pi*U_inf*x(1))))  ; ( U_inf*h-x(2)/2/pi* atan2((2*x(1)*h),(h^2-x(1)^2)))];
inc = fsolve(b_lambda, [1, 5]);

b = inc(1);
lambda = inc(2);


% stream function in coordinate cartesiane
psi_xy  =   @(x, y) U_inf*y+...
                                 + lambda/(2*pi) .* atan2(y, x+b)+...
                                 - lambda/(2*pi) .* atan2(y, x-b);

psi_impli = @(x, y, psi_c) psi_xy(x, y)-psi_c;


% Computational domain
x_span = 5;
y_span = 3;
x_dom = [-x_span:h:x_span];
y_dom = [-y_span:h:y_span];


%%
% psi_x_vect = psi_xy(x_dom(1), y_dom);
y_vect = y_dom;
A = zeros(length(y_dom), length(x_dom));
A(:,1) = y_vect

for i = 2:length(x_dom)
    
        psi_x = @(y) psi_impli(x_dom(i), y, psi_c);
        guess = A(:,end);
        
        psi_x = @(y) psi_impli(x0, y, b, l, psi_c);
        
        [y_vect, fval, exitflag, output] = fzero(psi_x_vect, guess);
        
        A = [A y_vect']
    
end


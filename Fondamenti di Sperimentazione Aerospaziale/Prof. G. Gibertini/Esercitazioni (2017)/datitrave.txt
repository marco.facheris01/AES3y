# File contenente misurazioni di deflessione della trave
# I dati sono espressi in N e mm


F [N]       Misure 1 [mm]       Misure 2 [mm]       Misure 3 [mm]

1.;         7.2;               7.1;                 7.3
2.;         13.7;               13.9;               13.3
3.;         19.2;               19.7;               19.4
4.;         26.3;               26.7;               27.1
# fine dati
close all
clear all

%% EX.1
p_n = [0 200 400 600 800 1000 800 600 400 200 0]'; %nominal pressure
p_m = [7 192 391 596 807 1022 816 606 399 203 10]'; %measured pressure
z = [min(p_n):1:max(p_n)];

P = polyfit(p_n,p_m,1);
Pz = polyval(P,z);

figure
plot(z, Pz, 'g')
hold on
plot (p_n, p_m, 'ko')

err = p_m - polyval(P,p_n)

devMax = max(err)
devMin = min(err)

% h = area(p+z,p-z);
% h.FaceColor = [0 0 1];
% h.FaceAlpha = 0.25;
% h.LineStyle = 'None';


plot(z, Pz+devMax, 'r')
plot(z, Pz-devMax, 'b')

legend('Calibration Curve', 'Measured Data', 'Max Deviation','Min Deviation')


%% EX.2
clear all

F = [0.5 1 1.5 2 2.5 3 3.5 4 3.5 3 2.5 2 1.5 1 0.5 0.5 1 1.5 2 2.5 3 3.5 4 3.5 3 3.5 2.5 2 1.5 1 0.5]'/9.81; % Force [N]
x = [1.305679 2.558065 3.783804 5.009543 6.235282 7.540961 8.633468 9.9125 9.139751 7.754133 6.581687 5.249362 3.996976 2.771237 1.385618 1.225739 2.611358 3.863743 5.142776 6.261929 7.514315 8.740054 9.885854 9.033166 7.834073 8.84664 6.55504 5.302655 4.076915 2.771237 1.412265]'*1e-3; % Displacement [m]
P = polyfit(F,x,1);
z = [min(F):.01:max(F)];
Pz = polyval(P,z);

figure
plot(z, Pz, 'g')
hold on
plot (F, x, 'ko')

k = P(1) % [N/m]




%% EX.3
% V_s = V*e^-(t/tau)
V = 10; %[V]
R = 100*1e+3; %[Ohm]
t = [0 10 20 30 40 50]';
V_s = [10.1040 6.0680 3.6775 2.2326 1.3790 0.8186]';


P = polyfit(t,log(V_s),1);
figure
semilogy(t, V_s, 'o')

tau = -1/P(1) %[s] (P(1) is the slope of the semilogy line =-1/tau)
C = tau/R %[F]

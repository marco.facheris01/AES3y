function Salpha=rotate_stiffness(S,angle_rot)
n = sin(angle_rot*pi/180);
m = cos(angle_rot*pi/180);
T =[m*m 	n*n	  2*m*n,
    n*n 	m*m  -2*m*n,
    -m*n  m*n	 (m*m-n*n)];
Salpha=inv(T)*S*inv(T)';

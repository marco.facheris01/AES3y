function eps_x=Rotazione_Deformazioni(eps_X,alfa)
n = sin(alfa*pi/180);
m = cos(alfa*pi/180);
J =[m*m 	n*n	  2*m*n,
    n*n 	m*m      -2*m*n,
    -m*n        m*n	 (m*m-n*n)];
eps_x=inv(J)'*eps_X;


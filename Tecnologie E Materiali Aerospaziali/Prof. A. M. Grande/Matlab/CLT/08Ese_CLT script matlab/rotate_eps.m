function epsalpha=rotate_eps(eps,angle_rot)

n = sin(angle_rot*pi/180);
m = cos(angle_rot*pi/180);
T =[m*m 	n*n	  2*m*n,
    n*n 	m*m  -2*m*n,
   -m*n   m*n	 (m*m-n*n)];
R =[1 0 0, 
    0 1 0, 
    0 0 2 ];
eps=inv(R)*eps;
epsalpha=T*eps;
epsalpha=R*epsalpha;
